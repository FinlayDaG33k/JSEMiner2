﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JseMiner2.JSEMiner {
    public class JSEMinerBlock {
        public string nonce { get; set; }
        public string hash { get; set; }
        public string previousHash { get; set; }
        public string version { get; set; }
        public long startTime { get; set; }
        public int frequency { get; set; }
        public int size { get; set; }
        public int difficulty { get; set; }
        public bool mainChain { get; set; }
        public string server { get; set; }
        public int block { get; set; }
        public string data { get; set; }
        public int hashSubmissions { get; set; }
    }
}
