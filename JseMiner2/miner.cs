﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JseMiner2.JSEMiner {
    public class Miner {
        public JSEMinerBlock CurrentBlock { get; private set; }
        public Miner() {

        }

        public async Task RequestNewBlock() {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.PostAsync("https://load.jsecoin.com/server/request/", new StringContent("1"));
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            CurrentBlock = JsonConvert.DeserializeObject<JSEMinerBlock>(responseBody);
            Console.WriteLine("JSEcoin. New Block Received.");
            Debugger.Break();
        }
    }
}
